variable "create_default_admin_user" {
  type    = bool
  default = true
}

variable "default_admin_user_name" {
  type    = string
  default = "admin"
}

variable "default_admin_user_path" {
  type    = string
  default = "/bot/"
}

variable "default_admin_user_force_destroy" {
  type    = bool
  default = true
}

variable "create_default_admin_group" {
  type    = bool
  default = true
}

variable default_admin_group_name {
  type    = string
  default = "Admin"
}

variable default_admin_group_path {
  type    = string
  default = "/groups/"
}

variable "admin_users" {
  type    = list(string)
  default = []
}

variable "pgp_key" {
  type    = string
  default = null
}
