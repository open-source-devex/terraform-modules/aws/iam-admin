resource "aws_iam_user" "default_admin" {
  count = var.create_default_admin_user ? 1 : 0

  name          = var.default_admin_user_name
  path          = var.default_admin_user_path
  force_destroy = var.default_admin_user_force_destroy
}

resource "aws_iam_access_key" "admin" {
  count = var.create_default_admin_user ? 1 : 0

  user    = aws_iam_user.default_admin[count.index].name
  pgp_key = var.pgp_key
}
