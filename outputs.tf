output "user_admin" {
  value = var.create_default_admin_user ? aws_iam_user.default_admin[0].name : "not created"
}

output "user_admin_arn" {
  value = var.create_default_admin_user ? aws_iam_user.default_admin[0].arn : "not created"
}

output "group_admin" {
  value = var.create_default_admin_group ? aws_iam_group.default_admin[0].name : "not created"
}

output "aws_access_key_id" {
  value = join("", aws_iam_access_key.admin.*.id)
}

output "aws_secret_access_key_encrypted" {
  value = try(join("", aws_iam_access_key.admin.*.encrypted_secret), "")
}
