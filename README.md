# aws-group-admin

Terraform module to provision a default IAM admin user and group.

## Usage

```hcl-terraform
module "iam_admin" {
  source = "git://gitlab.com/open-source-devex/terraform-modules/aws/iam-admin.git?ref=v0.0.7"

  admin_users = [
    "aws_iam_user.bender.name"
  ]
}
```

| variable | type | usage |
| -------- | ---- | ----- |
| admin_users | list | names of aws users to be added to the admin group |

## Terraform versions

The latest terraform version is 0.12 and that is the one supported in the mainline of this module.
For terraform 0.11 see branch `master-0.11` in this same repo.
