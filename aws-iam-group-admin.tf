data "aws_caller_identity" "current" {}

resource "aws_iam_group" "default_admin" {
  count = var.create_default_admin_group ? 1 : 0

  name = var.default_admin_group_name
  path = var.default_admin_group_path
}

resource "aws_iam_group_membership" "admin" {
  count = var.create_default_admin_group ? 1 : 0

  name = "group-admin"

  users = compact(concat([var.create_default_admin_user ? aws_iam_user.default_admin[0].name : ""], var.admin_users))
  group = aws_iam_group.default_admin[count.index].name
}

resource "aws_iam_policy" "admin_change_password" {
  count = var.create_default_admin_group ? 1 : 0

  name        = "admin-change-password"
  path        = "/"
  description = "Policy to enable admins to change their password"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "iam:GetAccountPasswordPolicy",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "iam:ChangePassword",
      "Resource": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/$${aws:username}"
    }
  ]
}
EOF
}

resource "aws_iam_group_policy_attachment" "admin_full_access" {
  count = var.create_default_admin_group ? 1 : 0

  group      = aws_iam_group.default_admin[count.index].name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_group_policy_attachment" "admin_change_password" {
  count = var.create_default_admin_group ? 1 : 0

  group      = aws_iam_group.default_admin[count.index].name
  policy_arn = aws_iam_policy.admin_change_password[count.index].arn
}

